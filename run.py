#!/usr/bin/env python
"""The run script"""
import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fsl_fslhd.main import run
from fw_gear_fsl_fslhd.parser import parse_config

log = logging.getLogger(__name__)


def main(gear_context):
    # Parses nifti info
    (
        nifti_file_path,
        nifti_file_name,
        output_json,
        parse_descrip,
        existing_info,
    ) = parse_config(gear_context)

    # process
    nifti_header, nifti_file_descrip, nifti_existing_info = run(
        nifti_file_path,
        nifti_file_name,
        output_json,
        parse_descrip,
        existing_info,
        outbase=gear_context.output_dir,
    )

    return 0, nifti_file_name, nifti_header, nifti_file_descrip, nifti_existing_info


if __name__ == "__main__":
    with GearToolkitContext() as context:
        try:
            context.init_logging()
            (
                status,
                nifti_file_name,
                nifti_header,
                nifti_file_descrip,
                nifti_existing_info,
            ) = main(context)

            info = nifti_existing_info
            info.update({"fslhd": nifti_header})
            info["fslhd"]["descrip"] = "".join(nifti_file_descrip)
            context.update_file_metadata(nifti_file_name, info=info)

        except Exception as exc:
            log.exception(exc)
            status = 1

    sys.exit(status)
