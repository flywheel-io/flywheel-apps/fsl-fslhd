"""Module to test parser.py"""
import os.path
from pathlib import Path
from re import search

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fsl_fslhd.parser import parse_config

ASSETS_ROOT = Path(__file__).parent / "assets"


def test_parse_config():

    context = GearToolkitContext(config_path=ASSETS_ROOT / "config.json", input_args=[])

    (
        nifti_file_path,
        nifti_file_name,
        output_json,
        parse_descrip,
        existing_info,
    ) = parse_config(context)
    assert search("nii", nifti_file_name)
    assert type(nifti_file_name) is str
    assert type(nifti_file_path) is str
    assert nifti_file_name == "example4d.nii.gz"
    assert nifti_file_path == os.path.dirname(nifti_file_path) + "/" + os.path.basename(
        nifti_file_path
    )
