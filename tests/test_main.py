import json
from pathlib import Path

from nibabel.testing import data_path

from fw_gear_fsl_fslhd.main import run

ASSETS_ROOT = Path(__file__).parent / "assets"


def test_run(tmpdir):
    nifti_file_path = data_path + "/example4d.nii.gz"
    nifti_file_name = "/example4d.nii.gz"
    existing_info = {}
    output_json = {"out_json"}
    parse_descrip = {"parse_descrip"}
    with open(ASSETS_ROOT / "full_header.json", "r") as fd:
        # Note: full_header.json was generated from the docker image
        # It turns out that the keys are not exactly the dependent if the output is generated from
        # MacOS
        expected_res = json.load(fd)

    res = run(
        nifti_file_path,
        nifti_file_name,
        output_json,
        parse_descrip,
        existing_info,
        tmpdir,
    )

    deny_key_list = ["header_filename", "image_filename", "filename", "pixdim0"]

    for k in res[0]:
        if k in expected_res and k not in deny_key_list:
            assert expected_res.get(k) == res[0].get(
                k
            )  # res as tuple needs to get access using [0]
