"""Test interfaces module"""

from pathlib import Path
from unittest.mock import patch

import pytest

from fw_gear_fsl_fslhd.interfaces import (
    FSLHD,
    _extract_nifti_text_header,
    _extract_nifti_xml_header,
    assign_type,
    get_nifti_header,
)

ASSETS_ROOT = Path(__file__).parent / "assets"


def test_FSLHD_interface():
    fslhd = FSLHD()
    assert hasattr(fslhd.inputs, "x_param")
    assert hasattr(fslhd.inputs, "out_file")
    assert fslhd.cmd == "fslhd"


@pytest.mark.parametrize(
    "val,expected",
    [
        (["1", "2"], [1, 2]),
        (["1.0", "2.0"], [1.0, 2.0]),
        (["a", "b"], ["a", "b"]),
        ("a", "a"),
        ("1", 1),
        ("1.0", 1.0),
        ("", ""),
    ],
)
def test_assign_type(val, expected):
    assert assign_type(val) == expected


def test_extract_nifti_text_header():
    res = _extract_nifti_text_header(ASSETS_ROOT / "sample.txt")

    assert res == {
        "sizeof_hdr": 348,
        "data_type": "INT16",
        "dim0": 4,
        "dim1": 128,
        "dim2": 96,
    }


def test_extract_nifti_xml_header():
    res = _extract_nifti_xml_header(ASSETS_ROOT / "sample.xml")

    assert res == {"image_offset": 416, "ndim": 4, "nx": 128, "ny": 96}


@patch("fw_gear_fsl_fslhd.interfaces._extract_nifti_text_header")
@patch("fw_gear_fsl_fslhd.interfaces._extract_nifti_xml_header")
def test_get_nifti_header(MockXMLHeader, MockTextHeader):
    MockXMLHeader.return_value = {"some": "dict", "over": "write"}
    MockTextHeader.return_value = {"toto": "titi", "over": "written"}

    res = get_nifti_header(None, None)

    assert res == {"some": "dict", "toto": "titi", "over": "written"}
