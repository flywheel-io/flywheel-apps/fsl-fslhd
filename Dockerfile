FROM neurodebian:bionic

MAINTAINER Flywheel <support@flywheel.io>

# Install FSL
RUN echo deb http://neurodeb.pirsquared.org data main contrib non-free >> /etc/apt/sources.list.d/neurodebian.sources.list
RUN echo deb http://neurodeb.pirsquared.org bionic main contrib non-free >> /etc/apt/sources.list.d/neurodebian.sources.list
RUN apt-get update \
    && apt-get install -y fsl-core \
    && rm -rf /var/cache/apt/
RUN echo ". /etc/fsl/5.0/fsl.sh" >> /root/.bashrc

RUN apt-get install -y \
    python-pip\
    python3-pip\
    git \
    curl \
    build-essential \
    zlib1g-dev \
    libncurses5-dev \
    libgdbm-dev \
    libnss3-dev \
    libssl-dev \
    libsqlite3-dev \
    libreadline-dev \
    libffi-dev curl \
    libbz2-dev \
    libpcap-dev \
    make

RUN curl -O https://www.python.org/ftp/python/3.9.4/Python-3.9.4.tgz &&\
    tar -xf Python-3.9.4.tgz

# Since poetry needs python 3.8 or higher and Debian will only update to
# 3.7, use the following workaround to force install of python 3.9
# NOTE: did not use the altinstall option as geartk v0.2.0 is not
# found unless py3.9 becomes the default
# Also, run all the make steps together
RUN cd Python-3.9.4 &&\
    ./configure &&\
    make -j 4 &&\
    make install

RUN pip3 install "poetry==1.1.6"

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Configure FSL environment
ENV FSLDIR=/usr/share/fsl/5.0
ENV FSL_DIR="${FSLDIR}"
ENV PATH=/usr/lib/fsl/5.0:$PATH
ENV POSSUMDIR=/usr/share/fsl/5.0
ENV LD_LIBRARY_PATH=/usr/lib/fsl/5.0:$LD_LIBRARY_PATH
ENV FSLOUTPUTTYPE=NIFTI_GZ

# Installing main dependencies
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev


# Installing the current project (most likely to change, above layer can be cached)
# Note: poetry requires a README.md to install the current project
COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_fsl_fslhd $FLYWHEEL/fw_gear_fsl_fslhd

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]

