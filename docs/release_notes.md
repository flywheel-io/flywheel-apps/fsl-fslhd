# Release notes
## 1.1.3_5.0

__Fixes__

* category changed to "utility"

* .metadata.json format
  
* output_json creation

* restore parse_descrip option 

## 1.1.2_5.0

__Enhancements__

* Add `debug` option.

__Maintenance__

* Refactor the gear with poetry and nipype.