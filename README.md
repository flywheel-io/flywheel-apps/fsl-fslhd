# FSLHD Gear
A Flywheel Gear for implementing FSL's fslhd for obtaining metadata info from NIfTI.

### Gear Inputs
NIFTI image

An input file (*.nii or *.nii.gz) is required.

### Configuration
* __debug__ (boolean, default False): Include debug statements in output.

### Output
Metadata from the header of the NIFTI image.

## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
